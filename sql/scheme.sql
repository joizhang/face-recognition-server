-- 用户表
DROP TABLE IF EXISTS t_user;

CREATE TABLE t_user (
	id int NOT NULL AUTO_INCREMENT,
	username varchar(32) NOT NULL,
	cell_phone_number varchar(32) NOT NULL,
	create_date datetime NOT NULL,
	PRIMARY KEY (`id`)
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET = utf8mb4;

-- 照片表
DROP TABLE IF EXISTS t_photo;

CREATE TABLE t_photo (
	id int NOT NULL AUTO_INCREMENT,
	photo_name varchar(512) NOT NULL,
	embedding blob NOT NULL,
	user_id int NOT NULL COMMENT '用户ID',
	model_id int NOT NULL COMMENT '人脸识别模型ID',
	model_name varchar(32) NOT NULL COMMENT '人脸识别模型名称',
	PRIMARY KEY (`id`)
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET = utf8mb4;

-- 人脸任务表
DROP TABLE IF EXISTS t_face_task;

CREATE TABLE t_face_task (
	id int NOT NULL AUTO_INCREMENT,
	task_name varchar(32) NOT NULL,
	PRIMARY KEY (`id`)
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET = utf8mb4;
INSERT INTO t_face_task VALUES (1, 'Face Recognition');
INSERT INTO t_face_task VALUES (2, 'Face Anti-Spoofing');
INSERT INTO t_face_task VALUES (3, 'Face Forgery Detection');

-- 人脸模型表
DROP TABLE IF EXISTS t_face_model;

CREATE TABLE t_face_model (
    id int NOT NULL AUTO_INCREMENT,
    model_name varchar(32) NOT NULL,
    task_id int NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET = utf8mb4;
INSERT INTO t_face_model VALUES (1, 'FaceNet', 1);
INSERT INTO t_face_model VALUES (2, 'CosFace', 1);
INSERT INTO t_face_model VALUES (3, 'ArcFace', 1);
INSERT INTO t_face_model VALUES (4, 'Dlib', 1);

-- Deepfake表
DROP TABLE IF EXISTS t_deepfake_video;

CREATE TABLE t_deepfake_video (
	id int NOT NULL AUTO_INCREMENT,
	video_name varchar(256) NOT NULL COMMENT '视频名称',
	status TINYINT NOT NULL COMMENT '状态：0-未检测，1-正在检测，2-完成检测，3-执行失败',
	progress TINYINT NOT NULL COMMENT '检测进度[0, 100]',
	probability FLOAT NOT NULL COMMENT '伪造概率',
	create_date datetime NOT NULL COMMENT '上传时间',
	PRIMARY KEY (`id`)
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET = utf8mb4;

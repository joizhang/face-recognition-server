from flask import Blueprint

bp = Blueprint('api', __name__)

from . import foo
from . import user
from . import face
from . import photo
from . import deepfake
from . import deepfake_detection

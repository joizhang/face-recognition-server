import unittest

import torch


class EnvTestCase(unittest.TestCase):

    def test_pytorch_env(self):
        self.assertTrue(torch.cuda.is_available())
        print(torch.cuda.get_device_properties('cuda:0').total_memory)

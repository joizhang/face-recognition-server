from app import create_app

# gunicorn -w 1 --worker-class=gevent -b 0.0.0.0:5000 --daemon wsgi:app
app = create_app("production")

from cryptography.fernet import Fernet

KEY = b'9cGYWV01y_PG4EF35ebyLmc222uhimfinpUBzTRX97s='

F = Fernet(KEY)


def encrypt(data: str):
    return str(F.encrypt(bytes(data, encoding="utf8")), encoding='utf8')


def decrypt(token):
    return str(F.decrypt(bytes(token, encoding="utf8")), encoding='utf8')

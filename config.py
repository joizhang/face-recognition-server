import os

from dotenv import load_dotenv

DEPLOY_ENV = os.environ.get("DEPLOY", "development")
BASE_DIR = os.path.abspath(os.path.dirname(__file__))
load_dotenv(os.path.join(BASE_DIR, f'.env'))

CONFIGS = {}
CONFIG_OPTIONS = [
    'SQLALCHEMY_DATABASE_URI',
    'FACE_DATA_PATH',
    # 'FACE_NET_MODEL_PATH',
    # 'FACE_NET_TORCH_HOME',
    'ANTI_SPOOFING',
    'ANTI_SPOOFING_TWO_STREAM_VIT_PATH',
    'DEEPFAKE_DETECTION',
    'DEEPFAKE_DETECTION_SIFDNET_PATH'
]


def get_config(key):
    value = CONFIGS.get(key, os.getenv(key))
    if "PATH" in key:
        value = os.path.join(BASE_DIR, value)
    return value


class Config:
    ENV = DEPLOY_ENV
    LOG_TO_STDOUT = (os.getenv("LOG_TO_STDOUT") == 'true')
    SQLALCHEMY_ECHO = (os.getenv("SQLALCHEMY_ECHO") == 'true')
    SQLALCHEMY_TRACK_MODIFICATIONS = (os.getenv("SQLALCHEMY_TRACK_MODIFICATIONS") == 'true')
    FACE_NET_MODEL_PATH = os.getenv('FACE_NET_MODEL_PATH')
    FACE_NET_TORCH_HOME = os.getenv('TORCH_HOME')

    @staticmethod
    def init_app(app):
        pass


class DevelopmentConfig(Config):
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = os.getenv('SQLALCHEMY_DATABASE_URI')

    @staticmethod
    def init_app(app):
        print('THIS APP IS IN DEBUG MODE. YOU SHOULD NOT SEE THIS IN PRODUCTION.')


class TestingConfig(Config):
    TESTING = True
    SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(BASE_DIR, 'app.sqlite')
    WTF_CSRF_ENABLED = False

    @staticmethod
    def init_app(app):
        print('THIS APP IS IN TESTING MODE. YOU SHOULD NOT SEE THIS IN PRODUCTION.')


class ProductionConfig(Config):
    DEBUG = False
    USE_RELOADER = False
    SQLALCHEMY_DATABASE_URI = os.getenv('SQLALCHEMY_DATABASE_URI')

    @staticmethod
    def init_app(app):
        pass


app_config = {
    'development': DevelopmentConfig,
    'testing': TestingConfig,
    'production': ProductionConfig,
    'default': DevelopmentConfig,
}

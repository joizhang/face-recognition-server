# face-recognition-server

## 介绍
Face recognition server end

## 软件架构
软件架构说明


## 安装教程

在本地运行该项目：
```
  1. 克隆项目：

        git clone https://gitee.com/HNUDLG/face-recognition-server.git
        cd face-recognition-server

  2. 创建 conda 环境：

        conda create -n face python=3.8
        conda activate face
        
  3. 安装依赖：
        
        conda install pytorch torchvision torchaudio cudatoolkit=11.3 -c pytorch
        pip install -r requirements.txt
        # Linux Only
        pip install gunicorn

  4. 运行程序：

        flask run 
        
  5. 生产环境部署：
  
        export TORCH_HOME=/home/xxx/data/torch_home/hub
        export FLASK_CONFIG=production
        gunicorn -w 4 --worker-class=gevent -b 0.0.0.0:5000 --daemon wsgi:app
```

## 参与贡献

1. Fork 本仓库
2. 添加修改代码
3. 提交代码
4. 新建 Pull Request

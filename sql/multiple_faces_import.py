import os
from options import parse_args
from app import create_app, db
from app.model import User, Photo


def main():
    args = parse_args()
    lfw_all_path = os.path.join(args.root_dir, 'lfw_all')
    names = open('multiple_faces_statistics.dat').readlines()
    create_app('testing')
    cell_phone_number = 12345678900
    for i, name in enumerate(names):
        name_split = name.split(',')
        assert len(name_split) == 2
        user = User()
        user.username = name_split[0]
        user.cell_phone_number = cell_phone_number + i + 1
        db.session.add(user)
        user = User.query.filter_by(name=user.username, cell_phone_number=user.cell_phone_number).first()




if __name__ == '__main__':
    main()

import collections
import itertools
import unittest

import cv2
import numpy as np
from PIL import Image

from app.recognition.deepfake_detection_sifdnet import DeepfakeDetectionSIFDNet
from app.recognition.face_detector import FacenetDetector
from app.utils import file_utils
from app.utils.image_utils import ndarray_error_threshold, crop_and_resize


class DeepfakeDetectionSIFDNetTestCase(unittest.TestCase):

    def test_prediction(self):
        video_path = str(file_utils.VIDEO_FOLDER / '000_003-1.mp4')
        deepfake_detection_sifdnet = DeepfakeDetectionSIFDNet.instance()
        deepfake_detection_sifdnet.create_model()

        frames, fps = file_utils.get_video_frames_and_fps(video_path)
        detector = FacenetDetector(landmarks=False)
        mask_frames = []
        fake_probs = []
        sliced = itertools.islice(frames.items(), int(fps))
        # sliced_o = collections.OrderedDict(sliced)
        for index, frame in sliced:
            # h, w = frame.shape[:2]
            # if index > 50:
            #     break
            mask_frame = np.zeros_like(frame)
            resized_image = Image.fromarray(frame)
            resized_image = resized_image.resize(size=(resized_image.size[0] // 2, resized_image.size[1] // 2))
            bboxes = detector.detect_faces(resized_image)
            frame_fake_prob_sum = 0.
            for bbox in bboxes:
                xmin, ymin, xmax, ymax = [int(b * 2) for b in bbox]
                w = xmax - xmin
                h = ymax - ymin
                p_h = h // 3
                p_w = w // 3
                crop = frame[max(ymin - p_h, 0):ymax + p_h, max(xmin - p_w, 0):xmax + p_w]
                fake_prob, masks_pred = deepfake_detection_sifdnet.get_predictions(crop)
                frame_fake_prob_sum += fake_prob
                masks_pred = ndarray_error_threshold(masks_pred)
                masks_pred = crop_and_resize(masks_pred, crop)
                masks_pred = masks_pred * 255
                masks_pred = np.expand_dims(masks_pred, axis=2)
                masks_pred = np.tile(masks_pred, (1, 1, 3))
                mask_frame[max(ymin - p_h, 0):ymax + p_h, max(xmin - p_w, 0):xmax + p_w] = masks_pred
            fake_probs.append(frame_fake_prob_sum / len(bboxes))
            mask_frames.append(mask_frame)

        shape = mask_frames[0].shape
        video_path = str(file_utils.VIDEO_FOLDER / 'video_tracked.webm')
        video_tracked = cv2.VideoWriter(video_path, cv2.VideoWriter_fourcc(*"vp80"), 25.0, (shape[1], shape[0]))
        for frame in mask_frames:
            video_tracked.write(cv2.cvtColor(frame, cv2.COLOR_RGB2BGR))
        video_tracked.release()

    def test_fps(self):
        video_path = str(file_utils.VIDEO_FOLDER / '000_003-1.mp4')
        capture = cv2.VideoCapture(video_path)
        fps = capture.get(cv2.CAP_PROP_FPS)
        self.assertEqual(25, fps)

        video_path = str(file_utils.VIDEO_FOLDER / 'id0_0000.mp4')
        capture = cv2.VideoCapture(video_path)
        fps = capture.get(cv2.CAP_PROP_FPS)
        self.assertEqual(30, fps)

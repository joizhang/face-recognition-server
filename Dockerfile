FROM python:3.8-slim-buster

COPY . /var/www/src/face-recognition-server
WORKDIR /var/www/src/face-recognition-server

RUN sed -i 's#http://deb.debian.org#https://mirrors.tuna.tsinghua.edu.cn#g' /etc/apt/sources.list
RUN apt update && apt install -y ffmpeg libsm6 libxext6
RUN pip3 install importlib-metadata poetry -i https://pypi.tuna.tsinghua.edu.cn/simple
RUN poetry config virtualenvs.create false
RUN poetry install

CMD ["gunicorn", "-w", "1", "--worker-class=gevent", "-b", "0.0.0.0:8085", "wsgi:app"]
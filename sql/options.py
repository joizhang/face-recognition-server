import argparse


def parse_args():
    parser = argparse.ArgumentParser(description="Parameters of preprocessing and statistical program")
    parser.add_argument("--root-dir", required=True, help="Root directory")
    parser.add_argument("--save", default=False, help="Save to file")
    args = parser.parse_args()
    return args
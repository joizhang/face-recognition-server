from flask import jsonify, current_app


def http_error_response(error):
    current_app.logger.error(error)
    error_msg = error.name
    if error.code == 400:
        error_msg = error.description
    return jsonify(success=False, message=error_msg, status_code=error.code)


def service_error_response(error):
    current_app.logger.error(error)
    return jsonify(success=False, message="Internal Server Error!", status_code=500)

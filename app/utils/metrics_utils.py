import numpy as np


def get_video_fake_prob(fake_probs: np.ndarray):
    real_num = len(fake_probs[fake_probs < 0.5])
    fake_num = len(fake_probs[fake_probs >= 0.5])
    if fake_num == 0 or real_num == 0:
        return np.sum(fake_probs) / fake_probs.shape[0]
    else:
        return 0.5 * (1 + (np.sum(fake_probs) / fake_probs.shape[0]))

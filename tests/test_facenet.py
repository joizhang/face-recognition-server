import pickle
import unittest

import numpy as np

from app.recognition.facenet import FaceNetTorch
from app.utils import file_utils


class FaceNetTorchTestCase(unittest.TestCase):

    def test_get_embedding(self):
        image_path = str(file_utils.PHOTO_FOLDER / 'Aaron_Eckhart_0001.jpg')
        image = file_utils.resize_image_path_to(image_path)
        face_net = FaceNetTorch.instance()
        face_net.create_model()
        embedding = face_net.get_embeddings(image)
        self.assertEqual(512, len(embedding))
        embedding_bin = pickle.dumps(embedding)
        print(embedding_bin)
        embedding_bin_load = pickle.loads(embedding_bin)
        result = np.sum(embedding - embedding_bin_load)
        self.assertEqual(0, result)

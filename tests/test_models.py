import time
import unittest

import numpy as np
import pickle

from app import create_app, db
from app.model import User, Photo


class UserModelTestCase(unittest.TestCase):

    def setUp(self) -> None:
        self.app = create_app('testing')
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()

    def tearDown(self):
        db.session.remove()
        # db.drop_all()
        self.app_context.pop()

    def test_embedding(self):
        embedding = str([1] * 2)
        self.assertEqual('[1, 1]', embedding)

    def test_insert_user(self):
        u = User()
        u.username = 'xxx' + str(int(time.time()))
        u.cell_phone_number = '12345678900'
        db.session.add(u)
        db.session.commit()
        embedding = np.array([1.0] * 128).dumps()
        p = Photo(name=u.username + '-' + u.cell_phone_number + '.jpg', embedding=embedding, user_id=u.id)
        db.session.add(p)
        db.session.commit()
        self.assertIsNotNone(u.id)


class PhotoModelTestCase(unittest.TestCase):
    def setUp(self) -> None:
        self.app = create_app('testing')
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()

    def tearDown(self):
        db.session.remove()
        # db.drop_all()
        self.app_context.pop()

    def test_query_photo(self):
        photo = Photo.query.filter_by(id=1).first()
        print(type(pickle.loads(photo.embedding)))
        self.assertEqual((128, ), pickle.loads(photo.embedding).shape)

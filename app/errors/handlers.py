from app import db
from app.errors import bp
from app.errors.errors import http_error_response, service_error_response
from werkzeug.exceptions import HTTPException


@bp.app_errorhandler(400)
def bad_request_error(error):
    return http_error_response(error)


@bp.app_errorhandler(404)
def not_found_error(error):
    return http_error_response(error)


@bp.app_errorhandler(Exception)
def server_error(error):
    if not isinstance(error, HTTPException):
        db.session.rollback()
        return service_error_response(error)

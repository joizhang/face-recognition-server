import unittest

from app.utils.encryption_utils import encrypt, decrypt


class CryptoUtilsTestCase(unittest.TestCase):

    def test_encrypt(self):
        data_1 = 'test1-15922865715-1'
        token_1 = encrypt(data_1)
        data_2 = decrypt(token_1)
        self.assertEqual(data_1, data_2)

from flask import jsonify, request
from pymysql.err import MySQLError

from . import bp


@bp.route('/raise-error', methods=['GET'])
def raise_error():
    raise MySQLError()


@bp.route('/hello')
def hello():
    return 'hello'


@bp.route('/foo', methods=['POST'])
def foo():
    data = request.json
    # print(data)
    return jsonify(success=True, data=data, status_code=200)

from app import db
from datetime import datetime

from app.model.pagination import PaginatedAPIMixin


class User(db.Model, PaginatedAPIMixin):
    __tablename__ = 't_user'

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(32), nullable=False)
    cell_phone_number = db.Column(db.String(32), nullable=False)
    create_date = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)

    def __repr__(self):
        return '<User {}, {}, {}, {}>'.format(self.id, self.username, self.cell_phone_number, self.create_date)

    def to_dict(self):
        data = {
            'id': self.id,
            'username': self.username,
            'cell_phone_number': self.cell_phone_number,
            'create_date': self.create_date
        }
        return data

    def from_dict(self, data):
        for field in ['id', 'username', 'cell_phone_number']:
            if field in data:
                setattr(self, field, data[field])


class Photo(db.Model):
    __tablename__ = 't_photo'

    id = db.Column(db.Integer, primary_key=True)
    photo_name = db.Column(db.String(512), nullable=False)
    embedding = db.Column(db.BLOB(), nullable=False)
    user_id = db.Column(db.Integer, nullable=False)
    model_id = db.Column(db.Integer, nullable=False)
    model_name = db.Column(db.String(32), nullable=False)

    def __repr__(self):
        return '<Photo {}, {}, {}, {}>'.format(self.id, self.user_id, self.model_id, self.photo_name)


class FaceModel(db.Model):
    __tablename__ = 't_face_model'

    id = db.Column(db.Integer, primary_key=True)
    model_name = db.Column(db.String(32), nullable=False)
    task_id = db.Column(db.Integer, nullable=False)

    def __repr__(self):
        return '<FaceModel {}, {}, {}>'.format(self.id, self.task_id, self.model_name)


class DeepfakeVideo(db.Model, PaginatedAPIMixin):
    __tablename__ = 't_deepfake_video'

    id = db.Column(db.Integer, primary_key=True)
    video_name = db.Column(db.String(256), nullable=False)
    status = db.Column(db.Integer, nullable=False)
    progress = db.Column(db.Integer, nullable=False)
    probability = db.Column(db.Float, nullable=False)
    create_date = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)

    def __repr__(self):
        return '<Deepfake Video: {}, {}, {}, {}>'.format(self.id, self.video_name, self.status, self.progress)

    def to_dict(self):
        data = {
            'id': self.id,
            'video_name': self.video_name,
            'status': self.status,
            'progress': self.progress,
            'probability': self.probability,
            'create_date': self.create_date
        }
        return data

    def from_dict(self, data):
        for field in ['id', 'video_name', 'status', 'progress']:
            if field in data:
                setattr(self, field, data[field])

SHELL := /bin/bash

test:
	PYTHONPATH=. pytest tests/test_metrics_utils.py
	PYTHONPATH=. pytest tests/test_encryption_utils.py
	#PYTHONPATH=. pytest tests/test_models.py
	PYTHONPATH=. pytest tests/test_file_utils.py
	PYTHONPATH=. pytest tests/test_facenet.py
	PYTHONPATH=. pytest tests/test_anti_spoofing_two_stream_vit.py
	PYTHONPATH=. pytest tests/test_deepfake_detection_sifdnet.py

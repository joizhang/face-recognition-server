"""
from sklearn.neighbors import KNeighborsClassifier


def face_net_compare(x, X, y, n=3):
    knn_classifier = KNeighborsClassifier(n_neighbors=n)
    knn_classifier.fit(X, y)
    return knn_classifier.predict(x)
"""

import numpy as np


def face_net_compare(embedding, x, y, threshold=1.0):
    distances = np.sqrt(np.sum(np.square(x - embedding), axis=1))
    min_distance_id = np.argmin(distances)
    assert isinstance(min_distance_id, np.int64), 'Type of min_distance_id is not int.'
    min_distance = distances[min_distance_id]
    print("distance = %.8f | minimum distance id = %d" % (min_distance, min_distance_id))
    if min_distance > threshold:
        return 0
    else:
        return y[min_distance_id]

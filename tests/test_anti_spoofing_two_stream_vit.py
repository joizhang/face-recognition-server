import unittest

import cv2

from app.recognition.anti_spoofing_two_stream_vit import AntiSpoofingTwoStreamVit
from app.utils import file_utils, retinex


class AntiSpoofingTwoStreamVitTestCase(unittest.TestCase):

    def test_get_y_prediction(self):
        image_path = str(file_utils.PHOTO_FOLDER / 'Aaron_Eckhart_0001.jpg')
        image = file_utils.resize_image_path_to(image_path, size=224)
        anti_spoofing_two_stream_vit = AntiSpoofingTwoStreamVit.instance()
        anti_spoofing_two_stream_vit.create_model()

        image_gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        image_gray = cv2.cvtColor(image_gray, cv2.COLOR_GRAY2BGR)
        msr = retinex.MSRCR(image_gray, sigma_list=[15, 80, 250], G=5.0, b=25.0, alpha=125.0,
                            beta=46.0, low_clip=0.01, high_clip=0.99)
        msr = cv2.cvtColor(msr, cv2.COLOR_BGR2GRAY)
        y_pred = anti_spoofing_two_stream_vit.get_prediction(x=image, msr=msr)
        self.assertEqual(0, y_pred)

import os
from abc import ABC, abstractmethod
from collections import OrderedDict
from typing import List

import cv2
import torch
from PIL import Image
from facenet_pytorch.models.mtcnn import MTCNN
from torch.utils.data import Dataset

os.environ["MKL_NUM_THREADS"] = "1"
os.environ["NUMEXPR_NUM_THREADS"] = "1"
os.environ["OMP_NUM_THREADS"] = "1"

cv2.ocl.setUseOpenCL(False)
cv2.setNumThreads(0)


class VideoFaceDetector(ABC):

    def __init__(self, **kwargs) -> None:
        super().__init__()

    @property
    @abstractmethod
    def batch_size(self) -> int:
        pass

    @abstractmethod
    def detect_faces(self, frames) -> List:
        pass


class FacenetDetector(VideoFaceDetector):

    def __init__(self, landmarks=False, device=torch.device("cuda:0")) -> None:
        super().__init__()
        self.landmarks = landmarks
        self.device = torch.device("cpu") if not torch.cuda.is_available() else device
        self.detector = MTCNN(margin=0, thresholds=[0.85, 0.95, 0.95], device=self.device)

    def detect_faces(self, frames) -> List:
        if self.landmarks:
            batch_boxes, batch_probs, batch_landmarks = self.detector.detect(frames, landmarks=self.landmarks)
            faces = []
            for boxes, landmarks in zip(batch_boxes, batch_landmarks):
                if boxes is not None:
                    faces.append({
                        'boxes': boxes.tolist(),
                        'landmarks': landmarks.tolist()
                    })
                else:
                    faces.append({
                        'boxes': None,
                        'landmarks': None
                    })
            return faces
        else:
            batch_boxes, *_ = self.detector.detect(frames, landmarks=self.landmarks)
            return [b.tolist() if b is not None else None for b in batch_boxes]

    @property
    def batch_size(self):
        return 8


class VideoDataset(Dataset):

    def __init__(self, videos) -> None:
        super().__init__()
        self.videos = videos

    def __getitem__(self, index: int):
        video = self.videos[index]
        capture = cv2.VideoCapture(video)
        frames_num = int(capture.get(cv2.CAP_PROP_FRAME_COUNT))
        frames = OrderedDict()
        for i in range(frames_num):
            capture.grab()
            success, frame = capture.retrieve()
            if not success:
                continue
            frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
            frame = Image.fromarray(frame)
            frame = frame.resize(size=[s // 2 for s in frame.size])
            frames[i] = frame
        return video, list(frames.keys()), list(frames.values())

    def __len__(self) -> int:
        return len(self.videos)

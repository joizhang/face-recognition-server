import unittest

import numpy as np

from app.utils import metrics_utils


class MetricsUtilsTestCase(unittest.TestCase):

    def test_get_video_fake_probability(self):
        probs = [0.9] * 5
        probs = np.array(probs)
        result = metrics_utils.get_video_fake_prob(probs)
        self.assertEqual(0.9, result)

        probs = [0.1] * 5
        probs = np.array(probs)
        result = metrics_utils.get_video_fake_prob(probs)
        self.assertEqual(0.1, result)

        probs = [0.1, 0.7, 0.8, 0.9]
        probs = np.array(probs)
        result = metrics_utils.get_video_fake_prob(probs)
        self.assertEqual(0.8125, result)

        probs = [0.1, 0.2, 0.8, 0.9]
        probs = np.array(probs)
        result = metrics_utils.get_video_fake_prob(probs)
        self.assertEqual(0.75, result)

        probs = [0.1, 0.1, 0.1, 0.6]
        probs = np.array(probs)
        result = metrics_utils.get_video_fake_prob(probs)
        self.assertEqual(0.6125, result)

        probs = [0.1, 0.1, 0.1, 0.7]
        probs = np.array(probs)
        result = metrics_utils.get_video_fake_prob(probs)
        self.assertEqual(0.625, result)

        probs = [0.1, 0.1, 0.1, 0.8]
        probs = np.array(probs)
        result = metrics_utils.get_video_fake_prob(probs)
        self.assertEqual(0.6375, result)
